# VM Workstation with MVS/Hercules and c3270 Terminal

Start MVS
CD back to the mvs directory and run ./mvs. This will start hercules in the hercules console and start mvs.

Log onto MVS
Start a separate terminal window and enter the following: c3270 127.0.0.1:3270

Hit enter once and use herc01 for a user name.
Use CUL8TR for a password.